package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Contract;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.contract.ContractRequest;
import com.episode6.myworkerapi.model.contract.ContractResponse;
import com.episode6.myworkerapi.repository.ContractRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContractService {
    private final ContractRepository contractRepository;

    /**
     * 계약 등록
     */
    public void setContract(BusinessMember businessMember, ContractRequest request) {
        contractRepository.save(new Contract.Builder(request,businessMember).build());
    }

    /**
     * 계약 상세보기
     */
    public ContractResponse getContract(long id) {
        Contract contract = contractRepository.findById(id).orElseThrow();
        return new ContractResponse.Builder(contract).build();
    }

    /**
     * 멤버 id 에 해당하는 계약 상세보기 (알바생 상세보기)
     */
    public ContractResponse getWorkerContract(long id) {
        Contract contract = contractRepository.findAllByBusinessMemberId(id);
        return new ContractResponse.Builder(contract).build();
    }

    /**
     * 계약 페이징 최신순 (관리자)
     */
    public ListResult<ContractResponse> getContracts(int pageNum){
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        Page<Contract> responsePage = contractRepository.findAllByOrderByIdDesc(pageRequest);

        List<ContractResponse> responseList = new LinkedList<>();
        for (Contract contract : responsePage.getContent()) responseList.add(new ContractResponse.Builder(contract).build());

        return ListConvertService.settingResult(
                responseList
                ,responsePage.getTotalElements()
                ,responsePage.getTotalPages()
                ,responsePage.getPageable().getPageNumber()
        );
    }

    /**
     * 사업장 id , 계약 페이징
     */
    public ListResult<ContractResponse> getOwnerContractPage(int pageNum,long id) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Contract> responsePage = contractRepository.findByBusinessMember_BusinessId(pageRequest,id);

        List<ContractResponse> responseList = new LinkedList<>();
        for (Contract contract : responsePage.getContent()) responseList.add(new ContractResponse.Builder(contract).build());

        return ListConvertService.settingResult(
                responseList
                ,responsePage.getTotalElements()
                ,responsePage.getTotalPages()
                ,responsePage.getPageable().getPageNumber()
        );
    }
}
