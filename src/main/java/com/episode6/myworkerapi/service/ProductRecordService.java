package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.entity.ProductRecord;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.productrecord.ProductRecordItem;
import com.episode6.myworkerapi.model.productrecord.ProductRecordRequest;
import com.episode6.myworkerapi.repository.ProductRecordRepository;
import com.episode6.myworkerapi.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductRecordService {
    private final ProductRepository productRepository;
    private final ProductRecordRepository productRecordRepository;

    /**
     * 재고 상품 등록 히스토리
     */
    public void setProductRecord(Product product, BusinessMember businessMember, ProductRecordRequest request) {
        productRecordRepository.save(new ProductRecord.Builder(product, businessMember, request).build());
        product.putProduct(businessMember,request);
        productRepository.save(product);
    }

    public ListResult<ProductRecordItem> getProductRecords(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<ProductRecord> productRecords = productRecordRepository.findAll(pageRequest);
        List<ProductRecordItem> result = new LinkedList<>();

        for (ProductRecord productRecord : productRecords) result.add(new ProductRecordItem.Builder(productRecord).build());
        return ListConvertService.settingResult(result, productRecords.getTotalElements(), productRecords.getTotalPages(), productRecords.getPageable().getPageNumber());
    }
}
