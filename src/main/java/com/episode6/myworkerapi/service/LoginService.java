package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.configure.JwtTokenProvider;
import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.MemberType;
import com.episode6.myworkerapi.exception.CMemberPasswordException;
import com.episode6.myworkerapi.exception.CMemberUsernameException;
import com.episode6.myworkerapi.model.login.LoginRequest;
import com.episode6.myworkerapi.model.login.LoginResponse;
import com.episode6.myworkerapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoginService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;

    //로그인 타입은 WEB or APP (WEB인 경우 토큰 유효시간 10시간, APP인 경우 토큰 유효시간 1년)
    public LoginResponse doLogin(MemberType memberType, LoginRequest request, String loginType) {
        //회원 정보가 없으면 던지기
        Member member = memberRepository.findByUsername(request.getUsername()).orElseThrow(CMemberUsernameException::new);
        if(!member.getMemberType().equals(memberType)) throw new CMemberUsernameException();
        //만약 일반회원이 최고 관리자용으로 로그인하려거나 이런 경우이므로 메세지는 회원정보가 없습니다로 일단 던짐
        if(!passwordEncoder.matches(request.getPassword(), member.getPassword())) throw new CMemberPasswordException();
        //만약 비밀번호가 틀리면 일치하지않습니다 던짐

        String token = jwtTokenProvider.createToken(String.valueOf(member.getUsername()),member.getMemberType().toString(), loginType);

        return new LoginResponse.Builder(token, member.getName()).build();
    }

}
