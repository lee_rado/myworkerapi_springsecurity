package com.episode6.myworkerapi.service;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.Request;
import com.episode6.myworkerapi.enums.ResultCode;
import com.episode6.myworkerapi.exception.CBusinessJoinOverlapException;
import com.episode6.myworkerapi.model.request.RefuseReasonRequest;
import com.episode6.myworkerapi.model.request.RequestItem;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.BusinessRepository;
import com.episode6.myworkerapi.repository.RequestRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RequestCreateService {
    private final RequestRepository requestRepository;
    private final BusinessRepository businessRepository;


    /**
     * 등록 요청 , 중복불가
     */
    public void setRequest(Business business) throws Exception{
        Optional<Request> optionalRequest = requestRepository.findByBusiness(business);
        if (optionalRequest.isEmpty()) {
            requestRepository.save(new Request.Builder(business).build());
        } else throw new CBusinessJoinOverlapException();
    }
    /**
     * 요청 수락 , 사업장 승인여부 true , 날짜변경 후 리퀘스트 삭제
     */
    public void putRequestAgree(long id) {
        Request request = requestRepository.findById(id).orElseThrow();
        Business business = businessRepository.findById(request.getBusiness().getId()).orElseThrow();
        business.putRequestAgree();

        businessRepository.save(business);

        requestRepository.deleteById(id);
    }

    /**
     * 사업장 요청 거절 , 거절 사유 등록
     */
    public void setRefuseReason(long id, RefuseReasonRequest refuseReasonRequest) {
        Request request = requestRepository.findById(id).orElseThrow();
        Business business = businessRepository.findById(request.getBusiness().getId()).orElseThrow();
        business.putRefuseReason(refuseReasonRequest);

        businessRepository.save(business);
        requestRepository.deleteById(id);

    }

    /**
     * 등록 요청 최신순 보기
     */
    public ListResult<RequestItem> getRequestPage(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);

        Page<Request> requestPage = requestRepository.findAllByOrderByIdDesc(pageRequest);

        List<RequestItem> requestList = new LinkedList<>();
        for (Request request : requestPage.getContent()) {
            requestList.add(new RequestItem.Builder(request).build());
        }
            return ListConvertService.settingResult(
                    requestList
                    ,requestPage.getTotalElements()
                    ,requestPage.getTotalPages()
                    ,requestPage.getPageable().getPageNumber()
            );
    }
}
