package com.episode6.myworkerapi.service;


import com.episode6.myworkerapi.entity.*;
import com.episode6.myworkerapi.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeleteService {
    private final CommentRepository commentRepository;
    private final BoardRepository boardRepository;
    private final ReportBoardRepository reportBoardRepository;
    private final ReportCommentRepository reportCommentRepository;
    private final ProductRepository productRepository;
    private final ProductRecordRepository productRecordRepository;

    /**
     * 게시물 삭제 // 삭제 전 댓글 ,신고게시물 삭제
     */
    public void delBoard(long id) {

        List<Comment> originData = commentRepository.findByBoard_Id(id);
        // 코멘트의 보드id는 오리진 데이터임

        for (Comment comment : originData) {
            commentRepository.deleteById(comment.getId());
        }
        List<ReportBoard> reportBoards = reportBoardRepository.findByBoard_Id(id);

       for (ReportBoard reportBoard : reportBoards) {
           reportBoardRepository.deleteById(reportBoard.getId());
       }
        //  엔티티 가 오리진데이터 있는만큼 반복해 뭘? 레포지토리에서 코멘트 아이디 찾아서
        boardRepository.deleteById(id);
    }


    /**
     * 댓글 삭제 // 삭제 전 신고댓글 삭제
     */
    public void delComment(long id) {

        List<ReportComment> comments = reportCommentRepository.findByComment_Id(id);

        for (ReportComment reportComment : comments) {
            reportCommentRepository.deleteById(reportComment.getId());
        }
        commentRepository.deleteById(id);
    }

    /**
     * 상품 재고 삭제 // 삭제전에 상품 재고 히스토리 삭제
     */
    public void delProduct(long id) {
        Product product = productRepository.findById(id).orElseThrow();
        List<ProductRecord> productRecords = productRecordRepository.findByProduct(product);

        for (ProductRecord productRecord : productRecords) {
            productRecordRepository.deleteById(productRecord.getId());
        }
        productRepository.deleteById(id);
    }

}
