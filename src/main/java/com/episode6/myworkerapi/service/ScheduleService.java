package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.entity.Schedule;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.schedule.ScheduleEndWorkRequest;
import com.episode6.myworkerapi.model.schedule.ScheduleItem;
import com.episode6.myworkerapi.model.schedule.ScheduleRequest;
import com.episode6.myworkerapi.repository.ScheduleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ScheduleService {
    private final ScheduleRepository scheduleRepository;

    /**
     *
     * @param businessMember 사업장 알바
     * @param request 출근 날짜 시간 등록
     */
    public void setSchedule(BusinessMember businessMember, ScheduleRequest request) {
        Optional<Schedule> schedule = scheduleRepository.findByBusinessMemberAndDateWork(businessMember, LocalDate.now());
        LocalTime toTime = LocalTime.now();
        boolean bool = toTime.isAfter(LocalTime.parse(businessMember.getTimeScheduleStart()));
        request.setIsLateness(bool);
        if (schedule.isEmpty()) scheduleRepository.save(new Schedule.Builder(businessMember, request).build());
    }
//    일요일	Sunday
//    월요일	Monday
//    화요일	Tuesday
//    수요일	Wednesday
//    목요일	Thursday
//    금요일	Friday
//    토요일	Saturday

    /**
     * 출근 리스트 관리자용
     */
    public ListResult<ScheduleItem> getSchedules(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Schedule> schedules = scheduleRepository.findAllByOrderByIdDesc(pageRequest);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result, schedules.getTotalElements(), schedules.getTotalPages(), schedules.getPageable().getPageNumber());
    }

    /**
     *
     * @param business 사업장
     * @param pageNum 보고싶은 페이지
     * @return 사업장 알바생들 출퇴근 기록
     */
    public ListResult<ScheduleItem> getScheduleBusiness(Business business, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Schedule> schedules = scheduleRepository.findAllByBusinessMember_BusinessOrderByIdDesc(business, pageRequest);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result, schedules.getTotalElements(), schedules.getTotalPages(), schedules.getPageable().getPageNumber());
    }

    /**
     *
     * @param businessMember 사업장 알바생
     * @param pageNum 보고싶은 페이지
     * @return 알바생의 출퇴근 기록
     */
    public ListResult<ScheduleItem> getScheduleBusinessMember(BusinessMember businessMember, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<Schedule> schedules = scheduleRepository.findAllByBusinessMemberOrderByIdDesc(businessMember, pageRequest);
        List<ScheduleItem> result = new LinkedList<>();
        for (Schedule schedule : schedules) result.add(new ScheduleItem.Builder(schedule).build());
        return ListConvertService.settingResult(result, schedules.getTotalElements(), schedules.getTotalPages(), schedules.getPageable().getPageNumber());
    }

    /**
     * 퇴근 하기
     */
    public void putEndWork(BusinessMember businessMember, ScheduleEndWorkRequest request) {
        Schedule schedule = scheduleRepository.findByDateWorkAndBusinessMember(LocalDate.now(), businessMember);
        double todayWorkTime = (double) ChronoUnit.HOURS.between(LocalTime.parse(businessMember.getTimeScheduleStart()), LocalTime.now());

        if (todayWorkTime >= 10) {
            request.setTotalWorkTime(8D);
            request.setPlusWorkTime(todayWorkTime - 8);
        }
        else {
            request.setTotalWorkTime(todayWorkTime - 1);
            request.setPlusWorkTime(0D);
        }
        boolean bool = todayWorkTime >= 10;
        request.setIsOverTime(bool);
        schedule.putEndWork(request);
        scheduleRepository.save(schedule);
    }

    /**
     * 휴식 시작
     */
    public void putRecessStart(BusinessMember businessMember) {
        Schedule schedule = scheduleRepository.findByDateWorkAndBusinessMember(LocalDate.now(), businessMember);
        if (schedule.getRestStartTime() == null) schedule.putRecessStart();
        scheduleRepository.save(schedule);
    }

    /**
     *
     * @param id 출퇴근 삭제
     */
    public void delSchedule(long id) {
        scheduleRepository.deleteById(id);
    }
}
