package com.episode6.myworkerapi.service;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.entity.MemberAsk;
import com.episode6.myworkerapi.model.memberask.MemberAskItem;
import com.episode6.myworkerapi.model.memberask.MemberAskRequest;
import com.episode6.myworkerapi.model.memberask.MemberAskResponse;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.repository.MemberAskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberAskService {
    private final MemberAskRepository memberAskRepository;

    /**
     * @param id
     * @return
     */
    public MemberAsk getMemberAskData(long id) {
        return memberAskRepository.findById(id).orElseThrow();
    }

    /**
     * @param member  회원이 문의사항
     * @param request 등록
     */
    public void setMemberAsk(Member member, MemberAskRequest request) {
        memberAskRepository.save(new MemberAsk.Builder(member, request).build());
    }

    /**
     * @param pageNum 문의사항 리스트
     * @return 페이징으로 보기
     */
    public ListResult<MemberAskItem> getAsks(int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<MemberAsk> memberAsks = memberAskRepository.findAllByOrderByIdDesc(pageRequest);

        List<MemberAskItem> result = new LinkedList<>();
        for (MemberAsk memberAsk : memberAsks) result.add(new MemberAskItem.Builder(memberAsk).build());

        return ListConvertService.settingResult(result, memberAsks.getTotalElements(), memberAsks.getTotalPages(), memberAsks.getPageable().getPageNumber());
    }

    /**
     *
     * @param member 회원이
     * @param pageNum 보고싶은 페이지
     * @return 회원이 쓴 문의글
     */
    public ListResult<MemberAskItem> getAskMembers(Member member, int pageNum) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum);
        Page<MemberAsk> memberAsks = memberAskRepository.findAllByMemberOrderByIdDesc(member, pageRequest);

        List<MemberAskItem> result = new LinkedList<>();
        for (MemberAsk memberAsk : memberAsks) result.add(new MemberAskItem.Builder(memberAsk).build());
        return ListConvertService.settingResult(result, memberAsks.getTotalElements(), memberAsks.getTotalPages(), memberAsks.getPageable().getPageNumber());
    }

    /**
     * @param id 문의사항
     * @return 상세보기
     */
    public MemberAskResponse getAsk(long id) {
        MemberAsk originData = memberAskRepository.findById(id).orElseThrow();
        return new MemberAskResponse.Builder(originData).build();
    }

    /**
     * 문의사항 삭제
     */
    public void delAnswer(long id) {
        memberAskRepository.deleteById(id);

    }
}