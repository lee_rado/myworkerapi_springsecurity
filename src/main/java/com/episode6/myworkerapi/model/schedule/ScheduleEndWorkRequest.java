package com.episode6.myworkerapi.model.schedule;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ScheduleEndWorkRequest {
    private Double endLatitude;
    private Double endLongitude;

    private Double restTime;
    private Double plusWorkTime;
    private Double outWorkTime;
    private Double totalWorkTime;
    private Boolean isOverTime;
}
