package com.episode6.myworkerapi.model.askanswer;

import com.episode6.myworkerapi.entity.AskAnswer;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AskAnswerItem {
    private Long id;
    private Long memberId;
    private String memberName;
    private Long memberAskId;
    private String memberAskMemberName;
    private String memberAskTitle;
    private String answerTitle;
    private LocalDateTime dateAnswer;

    private AskAnswerItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.memberAskId = builder.memberAskId;
        this.memberAskMemberName = builder.memberAskMemberName;
        this.memberAskTitle = builder.memberAskTitle;
        this.answerTitle = builder.answerTitle;
        this.dateAnswer = builder.dateAnswer;
    }

    public static class Builder implements CommonModelBuilder<AskAnswerItem> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final Long memberAskId;
        private final String memberAskMemberName;
        private final String memberAskTitle;
        private final String answerTitle;
        private final LocalDateTime dateAnswer;

        public Builder(AskAnswer askAnswer) {
            this.id = askAnswer.getId();
            this.memberId = askAnswer.getMember().getId();
            this.memberName = askAnswer.getMember().getName();
            this.memberAskId = askAnswer.getMemberAsk().getId();
            this.memberAskMemberName = askAnswer.getMemberAsk().getMember().getName();
            this.memberAskTitle = askAnswer.getMemberAsk().getTitle();
            this.answerTitle = askAnswer.getAnswerTitle();
            this.dateAnswer = askAnswer.getDateAnswer();
        }
        @Override
        public AskAnswerItem build() {
            return new AskAnswerItem(this);
        }
    }
}
