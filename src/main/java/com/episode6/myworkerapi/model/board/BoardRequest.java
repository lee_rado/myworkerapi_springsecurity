package com.episode6.myworkerapi.model.board;


import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.enums.Category;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BoardRequest {
    private String title;
    private String content;
    private String boardImgUrl;
    private Category category;
}
