package com.episode6.myworkerapi.model.notice;

import com.episode6.myworkerapi.entity.Notice;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class NoticeResponse {
    private Long id;
    private Long memberId;
    private String memberType;
    private LocalDateTime dateNotice;
    private String title;
    private String content;
    private String noticeImgUrl;

    private NoticeResponse(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberType = builder.memberType;
        this.dateNotice = builder.dateNotice;
        this.title = builder.title;
        this.content = builder.content;
        this.noticeImgUrl = builder.noticeImgUrl;
    }

    public static class Builder implements CommonModelBuilder<NoticeResponse> {
        private final Long id;
        private final Long memberId;
        private final String memberType;
        private final LocalDateTime dateNotice;
        private final String title;
        private final String content;
        private final String noticeImgUrl;

        public Builder(Notice notice) {
            this.id = notice.getId();
            this.memberId = notice.getMember().getId();
            this.memberType = notice.getMember().getMemberType().getName();
            this.dateNotice = notice.getDateNotice();
            this.title = notice.getTitle();
            this.content = notice.getContent();
            this.noticeImgUrl = notice.getNoticeImgUrl();
        }
        @Override
        public NoticeResponse build() {
            return new NoticeResponse(this);
        }
    }
}
