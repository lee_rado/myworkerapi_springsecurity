package com.episode6.myworkerapi.model.product;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductRequest {
    private String productName;
    private Short minQuantity;
    private Short nowQuantity;
}
