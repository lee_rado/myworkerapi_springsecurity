package com.episode6.myworkerapi.model.request;


import com.episode6.myworkerapi.entity.RequestFix;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class FixItem {
    private Long id;
    private Long BusinessId;
    private String businessName;
    private String ownerName;
    private String businessPhoneNumber;
    private String ectMemo;

    private FixItem(Builder builder) {
        this.id = builder.id;
        this.BusinessId = builder.BusinessId;
        this.businessName = builder.businessName;
        this.ownerName = builder.ownerName;
        this.businessPhoneNumber = builder.businessPhoneNumber;
        this.ectMemo = builder.ectMemo;
    }

    public static class Builder implements CommonModelBuilder<FixItem> {
        private final Long id;
        private final Long BusinessId;
        private final String businessName;
        private final String ownerName;
        private final String businessPhoneNumber;
        private final String ectMemo;

        public Builder(RequestFix fix) {
            this.id = fix.getId();
            this.BusinessId = fix.getBusiness().getId();
            this.businessName = fix.getBusinessName();
            this.ownerName = fix.getOwnerName();
            this.businessPhoneNumber = fix.getBusinessPhoneNumber();
            this.ectMemo = fix.getEtcMemo();
        }
        @Override
        public FixItem build() {
            return new FixItem(this);
        }
    }
}
