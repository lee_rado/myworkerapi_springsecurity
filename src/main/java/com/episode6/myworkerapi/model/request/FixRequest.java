package com.episode6.myworkerapi.model.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FixRequest {
    private String businessName;
    private String ownerName;
    private String businessImgUrl;
    private String businessType;
    private String businessLocation;
    private String businessEmail;
    private String businessPhoneNumber;
    private String reallyLocation;
    private String ectMemo;
}
