package com.episode6.myworkerapi.model.business;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessLocationRequest {

    private String reallyLocation;

}
