package com.episode6.myworkerapi.model.report;


import com.episode6.myworkerapi.entity.ReportComment;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReportCommentItem {
    private Long commentId;
    private Long memberId;
    private String content;
    private LocalDateTime dateReport;


    private ReportCommentItem(Builder builder) {
        this.commentId = builder.commentId;
        this.memberId = builder.memberId;
        this.content = builder.content;
        this.dateReport = builder.dateReport;
    }

    public static class Builder implements CommonModelBuilder<ReportCommentItem> {
        private final Long commentId;
        private final Long memberId;
        private final String content;
        private final LocalDateTime dateReport;


        public Builder(ReportComment reportComment) {
            this.commentId = reportComment.getId();
            this.memberId = reportComment.getMember().getId();
            this.content = reportComment.getContent();
            this.dateReport = LocalDateTime.now();
        }
        @Override
        public ReportCommentItem build() {
            return new ReportCommentItem(this);
        }
    }
}


