package com.episode6.myworkerapi.model.common;


import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult {
    private List<T> list;
    private Long totalCount;
    private Integer totalPage;
    private Integer currentPage;
}
