package com.episode6.myworkerapi.model.login;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LoginResponse {
    private String token;
    private String name;

    private LoginResponse(Builder builder) {
        this.token = builder.token;
        this.name = builder.name;

    }

    public static class Builder implements CommonModelBuilder<LoginResponse> {
        private final String token;
        private final String name;

        public Builder(String token, String name) {
            this.token = token;
            this.name = name;
        }

        @Override
        public LoginResponse build() {
            return new LoginResponse(this);
        }
    }
}
