package com.episode6.myworkerapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberPasswordCheckRequest {
    private String password;

    private String passwordRe;
}
