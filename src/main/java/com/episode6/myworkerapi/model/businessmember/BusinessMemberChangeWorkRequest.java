package com.episode6.myworkerapi.model.businessmember;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessMemberChangeWorkRequest {
    private Boolean isWork;
}
