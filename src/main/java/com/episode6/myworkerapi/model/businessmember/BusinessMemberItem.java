package com.episode6.myworkerapi.model.businessmember;

import com.episode6.myworkerapi.entity.BusinessMember;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BusinessMemberItem {
    private Long id;
    private Long memberId;
    private String memberName;
    private Long businessId;
    private String businessName;
    private String position;
    private String isWork;
    private LocalDate dateIn;
    private LocalDate dateOut;
    private String etc;
    private String weekSchedule;
    private String timeScheduleStart;
    private String timeScheduleEnd;
    private String timeScheduleTotal;
    private String timeScheduleRest;

    private BusinessMemberItem(Builder builder) {
        this.id = builder.id;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.businessId = builder.businessId;
        this.businessName = builder.businessName;
        this.position = builder.position;
        this.isWork = builder.isWork;
        this.dateIn = builder.dateIn;
        this.dateOut = builder.dateOut;
        this.etc = builder.etc;
        this.weekSchedule = builder.weekSchedule;
        this.timeScheduleStart = builder.timeScheduleStart;
        this.timeScheduleEnd = builder.timeScheduleEnd;
        this.timeScheduleTotal = builder.timeScheduleTotal;
        this.timeScheduleRest = builder.timeScheduleRest;
    }

    public static class Builder implements CommonModelBuilder<BusinessMemberItem> {
        private final Long id;
        private final Long memberId;
        private final String memberName;
        private final Long businessId;
        private final String businessName;
        private final String position;
        private final String isWork;
        private final LocalDate dateIn;
        private final LocalDate dateOut;
        private final String etc;
        private final String weekSchedule;
        private final String timeScheduleStart;
        private final String timeScheduleEnd;
        private final String timeScheduleTotal;
        private final String timeScheduleRest;

        public Builder(BusinessMember businessMember) {
            this.id = businessMember.getId();
            this.memberId = businessMember.getMember().getId();
            this.memberName = businessMember.getMember().getName();
            this.businessId = businessMember.getBusiness().getId();
            this.businessName = businessMember.getBusiness().getBusinessName();
            this.position = businessMember.getPosition().getName();
            this.isWork = businessMember.getIsWork() ? "재직중" : "퇴직";
            this.dateIn = businessMember.getDateIn();
            this.dateOut = businessMember.getDateOut();
            this.etc = businessMember.getEtc();
            this.weekSchedule = businessMember.getWeekSchedule();
            this.timeScheduleStart = businessMember.getTimeScheduleStart();
            this.timeScheduleEnd = businessMember.getTimeScheduleEnd();
            this.timeScheduleTotal = businessMember.getTimeScheduleTotal();
            this.timeScheduleRest = businessMember.getTimeScheduleRest();
        }
        @Override
        public BusinessMemberItem build() {
            return new BusinessMemberItem(this);
        }
    }
}
