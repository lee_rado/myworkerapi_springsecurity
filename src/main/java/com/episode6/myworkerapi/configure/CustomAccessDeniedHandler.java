package com.episode6.myworkerapi.configure;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
//component : 기능의 묶음 / 세부적인 컴포넌트 정의 /무언가 기능을 하는 단위입니다 명찰 달아준 것

public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    //접근 거부 상황에서 발생하는 예외를 처리

    //로그인을 안했는데 권한을 요구할 때 거부하는 것
    //강의실에 외부인이 와서 앉는 경우
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        //Http는 데이터를 왔다갔다하기 위해 있는 것 : 데이터라고 생각하기
        //Servlet는 서빙이라고 생각하기
        //HttpServletRequest는 고객이 서버한테 요청하는 데이터를 서빙하는 것
        //HttpServletResponse는 서버가 고객한테 주는 데이터를 서빙하는 것
        //일방적으로 이런이런 메세지를 줄거니까 리스폰스를 잘 활용해야한다.
        //컨트롤러의 주소를 준다.
        response.sendRedirect("/exception/access-denied");

    }
}
