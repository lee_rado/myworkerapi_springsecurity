package com.episode6.myworkerapi.configure;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final UserDetailsService userDetailsService;
    @Value("${spring.jwt.secret}")
    private String secretKey;

    @PostConstruct
    protected  void init() {
        //Base64:옛날 암호화 기법=>단방향
        //암호화 하는 것
        secretKey = Base64.getEncoder().encodeToString(secretKey.getBytes());
    }

    //토큰생성
    //claim:주장하다
    //claim이 있으면 "나"라고 생각하기
    public String createToken(String username, String role, String type) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("role",role);
        Date now = new Date();
        //토큰 유효시간
        //1000 밀리세컨드 = 1초
        //기본으로 10시간 유효하게 설정해 줌 왜냐하면 아침에 출근해서 로그인하고 점심먹고 퇴근하면 대충 10시간이니까.
        //웹 토큰과 쿠키는 시간을 동일하게 맞춰야한다.
        //=> 쿠기 보관 시간이 5분이고 웹 토큰이 10시간이면 문제가 생긴다.
        //앱용 토큰 같은 경우 유효시간 1년으로 설정해 줌 . 앱에서 아침마다 로그인하라고 하면 짜증나니까
        long tokenValiMillisecond = 1000L * 60 * 60 * 10; //10시간
        if (type.equals("APP")) tokenValiMillisecond = 1000L * 60 * 60 * 24 *365; //1년
        return Jwts.builder() //빌더 : 쌓는 것
                .setClaims(claims) //나야~
                .setIssuedAt(now) //언제 발급됐어~
                .setExpiration(new Date(now.getTime() + tokenValiMillisecond)) //만료일 언제까지야~
                .signWith(SignatureAlgorithm.HS256, secretKey) //사인 위조방지
                .compact(); //컴팩트하게 만든다~
    }

    // 토큰을 분석하여 인증정보를 가져옴.
    //토큰을 받아서 인증 정보를 주겠다.
    //일회용 인증:Authentication
    public Authentication getAuthentication(String token) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(getUsername(token));
        return new UsernamePasswordAuthenticationToken(userDetails,"", userDetails.getAuthorities());
    }

    // 토큰을 파싱하여 username을 가져옴
    // 토큰 생성시 username은 subjecy에 넣은 것 꼭 확인
    // jwt 사이트 보면서 코드 이해하기
    public String getUsername(String token) {
        return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody().getSubject();
    }

    // 리졸브(resolve)는 프론트에서도 많이 쓰인다 (뷰 리졸브)
    // 프로미스를 받는다 : 결과값을 전달받는 것 (실패든 성공이든)
    // 프로미스 중 성공 값만 받은게 리졸브이다.
    public String resolveToken(HttpServletRequest request) {
        //rest api - header 인증방식에서 Bearer를 언제 사용하는지보기
        return request.getHeader(HttpHeaders.AUTHORIZATION);
    }

    // 유효시간을 검사하여 유효시간이 지났으면 false를 줌
    // exception 대신에 트라이 캐치만 쓰는 이유는 유지보수 측면 때문이다.
    // service에서 던져야하는데 보안에서 던지면 이곳 저곳에서 다 던지게 되면 에러를 어디서 찾게 될 지 모르기 때문이다.
    public boolean validateToken(String jwtToken) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(jwtToken);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (Exception e) {
            return false;
        }
    }

}
