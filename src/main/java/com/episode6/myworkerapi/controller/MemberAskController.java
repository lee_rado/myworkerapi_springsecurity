package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.memberask.MemberAskItem;
import com.episode6.myworkerapi.model.memberask.MemberAskRequest;
import com.episode6.myworkerapi.model.memberask.MemberAskResponse;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.MemberAskService;
import com.episode6.myworkerapi.service.MemberService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member-ask")
public class MemberAskController {
    private final MemberService memberService;
    private final MemberAskService memberAskService;

    @PostMapping("/join/member-id/{memberId}")
    @Operation(summary = "문의사항 등록")
    public CommonResult setMemberAsk(@PathVariable long memberId, @RequestBody MemberAskRequest request) {
        Member member = memberService.getMemberData(memberId);
        memberAskService.setMemberAsk(member,request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "문의사항 리스트로 보기 페이징")
    public ListResult<MemberAskItem> getAsks(@PathVariable int pageNum) {
        return ResponseService.getListResult(memberAskService.getAsks(pageNum),true);
    }

    @GetMapping("/all/member-id/{memberId}/{pageNum}")
    @Operation(summary = "회원이 쓴 문의사항 리스트로 보기 페이징")
    public ListResult<MemberAskItem> getAskMembers(@PathVariable long memberId, @PathVariable int pageNum) {
        Member member = memberService.getMemberData(memberId);
        return ResponseService.getListResult(memberAskService.getAskMembers(member, pageNum), true);
    }

    @GetMapping("/detail/member-ask-id/{memberAskId}")
    @Operation(summary = "문의사항 상세보기")
    public SingleResult<MemberAskResponse> getAsk(@PathVariable long memberAskId) {
        return ResponseService.getSingleResult(memberAskService.getAsk(memberAskId));
    }

    @DeleteMapping("/del/member-ask-id/{memberAskId}")
    @Operation(summary = "문의사항 삭제")
    public CommonResult delAnswer(@PathVariable long memberAskId) {
        memberAskService.delAnswer(memberAskId);
        return ResponseService.getSuccessResult();
    }
}
