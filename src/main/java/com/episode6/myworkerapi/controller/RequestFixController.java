package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Business;
import com.episode6.myworkerapi.model.request.FixItem;
import com.episode6.myworkerapi.model.request.FixRequest;
import com.episode6.myworkerapi.model.request.FixResponse;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.BusinessService;
import com.episode6.myworkerapi.service.RequestFixService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/request-fix")
public class RequestFixController {
    private final RequestFixService requestFixService;
    private final BusinessService businessService;

    @PostMapping("join/business-id/{businessId}")
    @Operation(summary = "사장님 수정 요청 / 수정 할 내용 기입 후 수정요청 / 기존값 보기")
    public CommonResult setRequestFix(@RequestBody FixRequest request, @PathVariable long businessId) {

        Business business = businessService.getBusinessData(businessId);
        requestFixService.setRequestFix(business,request);

       return ResponseService.getSuccessResult();

    }

    @PutMapping("put/fix-id/{fixId}")
    @Operation(summary = "수정 요창 수락 / 사업장 테이블 변경")
    public CommonResult putRequestFix(@PathVariable long fixId) {
        requestFixService.putRequestFix(fixId);

       return ResponseService.getSuccessResult();
    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "수정 요청 최신순 페이징")
    public ListResult<FixItem> getFixPage(@PathVariable int pageNum) {

        return requestFixService.getFixPage(pageNum);
    }

    @GetMapping("detail/fix-id/{fixId}")
    @Operation(summary = "수정 요청 자세히 보기 / 사장님 수정 폼")
    public SingleResult<FixResponse> getRequestFix(@PathVariable long fixId) {

        return ResponseService.getSingleResult(requestFixService.getRequestFix(fixId));
    }
}
