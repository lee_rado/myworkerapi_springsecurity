package com.episode6.myworkerapi.controller;

import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.model.notice.NoticeChangeRequest;
import com.episode6.myworkerapi.model.notice.NoticeItem;
import com.episode6.myworkerapi.model.notice.NoticeRequest;
import com.episode6.myworkerapi.model.notice.NoticeResponse;
import com.episode6.myworkerapi.service.MemberService;
import com.episode6.myworkerapi.service.NoticeService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/notice")
public class NoticeController {
    private final MemberService memberService;
    private final NoticeService noticeService;

    @PostMapping("/join/member-id/{memberId}")
    @Operation(summary = "공지사항 등록")
    public CommonResult setNotice(@PathVariable long memberId, @RequestBody NoticeRequest request) {
        Member member = memberService.getMemberData(memberId);
        noticeService.setNotice(member, request);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("/all/{pageNum}")
    @Operation(summary = "공지사항 리스트 보기 페이징")
    public ListResult<NoticeItem> getNotices(@PathVariable int pageNum) {
        return ResponseService.getListResult(noticeService.getNotices(pageNum),true);
    }

    @GetMapping("/detail/notice-id/{noticeId}")
    @Operation(summary = "공지사항 상세보기")
    public SingleResult<NoticeResponse> getNotice(@PathVariable long noticeId) {
        return ResponseService.getSingleResult(noticeService.getNotice(noticeId));
    }

    @PutMapping("/change/notice-id/{noticeId}")
    @Operation(summary = "공지사항 수정")
    public CommonResult putNotice(@PathVariable long noticeId, @RequestBody NoticeChangeRequest request) {
        noticeService.putNotice(noticeId, request);

        return ResponseService.getSuccessResult();
    }

    @DeleteMapping("/notice-id/{noticeId}")
    @Operation(summary = "공지사항 삭제")
    public CommonResult delNotice(@PathVariable long noticeId) {
        noticeService.delNotice(noticeId);

        return ResponseService.getSuccessResult();
    }
}
