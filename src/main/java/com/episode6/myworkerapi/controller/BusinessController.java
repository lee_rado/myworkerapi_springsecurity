package com.episode6.myworkerapi.controller;


import com.episode6.myworkerapi.entity.Member;
import com.episode6.myworkerapi.model.business.BusinessChangeRequest;
import com.episode6.myworkerapi.model.business.BusinessItem;
import com.episode6.myworkerapi.model.business.BusinessLocationRequest;
import com.episode6.myworkerapi.model.business.BusinessRequest;
import com.episode6.myworkerapi.model.common.CommonResult;
import com.episode6.myworkerapi.model.common.ListResult;
import com.episode6.myworkerapi.model.common.SingleResult;
import com.episode6.myworkerapi.service.BusinessService;
import com.episode6.myworkerapi.service.MemberService;
import com.episode6.myworkerapi.service.ResponseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/business")
public class BusinessController {
    private final BusinessService businessService;
    private final MemberService memberService;

    @PostMapping("join/member-id/{memberId}")
    @Operation(summary = "사업장 등록")
    public CommonResult setBusiness(@RequestBody BusinessRequest request, @PathVariable long memberId) {

        Member member = memberService.getMemberData(memberId);

        businessService.setBusiness(request,member);

        return ResponseService.getSuccessResult();
    }

    @GetMapping("all/{pageNum}")
    @Operation(summary = "사업장 최신순 페이징")
    public ListResult<BusinessItem> getBusinessPage(@PathVariable int pageNum) {
        return ResponseService.getListResult(businessService.getBusinessPage(pageNum), true);
    }

    @GetMapping("detail/business-id/{businessId}")
    @Operation(summary = "사업장 상세보기")
    public SingleResult getBusiness(@PathVariable long businessId) {

        return ResponseService.getSingleResult(businessService.getBusiness(businessId));
    }

    @PutMapping("put/business-id/{businessId}")
    @Operation(summary = "관리자용 사업장 수정")
    public CommonResult putBusiness(@RequestBody BusinessChangeRequest request, @PathVariable long businessId) {
        businessService.putBusiness(businessId,request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("putLocation/business-id/{businessId}")
    @Operation(summary = "사장님 실근무지 수정")
    public CommonResult putBusinessLocation(@RequestBody BusinessLocationRequest request, @PathVariable long businessId) {
        businessService.putBusinessLocation(businessId,request);

        return ResponseService.getSuccessResult();
    }

}
