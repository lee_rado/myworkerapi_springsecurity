package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.enums.Insurance;
import com.episode6.myworkerapi.enums.PayType;
import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.contract.ContractRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Contract {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessMemberId",nullable = false)
    private BusinessMember businessMember;

    @Column(nullable = false)
    private LocalDate dateContract;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 15)
    private PayType payType;

    @Column(nullable = false)
    private Double payPrice;

    @Column(nullable = false)
    private LocalDate dateWorkStart;

    @Column(nullable = false)
    private LocalDate dateWorkEnd;

    @Column(nullable = false)
    private Short restTime;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Insurance insurance;

    @Column(nullable = false)
    private String workDay;

    @Column(nullable = false)
    private Short workTime;

    @Column(nullable = false)
    private Short weekWorkTime;

    @Column(nullable = false)
    private Boolean isWeekPay;

    @Column(nullable = false)
    private Double mealPay;

    @Column(nullable = false, length = 10)
    private String wagePayment;

    @Column(length = 25)
    private String wageAccountNumber;

    @Column(nullable = false)
    private String contractCopyImgUrl;


    private Contract(Builder builder) {
        this.businessMember = builder.businessMember;
        this.dateContract = builder.dateContract;
        this.payType = builder.payType;
        this.payPrice = builder.payPrice;
        this.dateWorkStart = builder.dateWorkStart;
        this.dateWorkEnd = builder.dateWorkEnd;
        this.restTime = builder.restTime;
        this.insurance = builder.insurance;
        this.workDay = builder.workDay;
        this.workTime = builder.workTime;
        this.weekWorkTime = builder.weekWorkTime;
        this.isWeekPay = builder.isWeekPay;
        this.mealPay = builder.mealPay;
        this.wagePayment = builder.wagePayment;
        this.wageAccountNumber = builder.wageAccountNumber;
        this.contractCopyImgUrl = builder.contractCopyImgUrl;
    }

    public static class Builder implements CommonModelBuilder<Contract> {
        private final BusinessMember businessMember;
        private final LocalDate dateContract;
        private final PayType payType;
        private final Double payPrice;
        private final LocalDate dateWorkStart;
        private final LocalDate dateWorkEnd;
        private final Short restTime;
        private final Insurance insurance;
        private final String workDay;
        private final Short workTime;
        private final Short weekWorkTime;
        private final Boolean isWeekPay;
        private final Double mealPay;
        private final String wagePayment;
        private final String wageAccountNumber;
        private final String contractCopyImgUrl;

        public Builder(ContractRequest contractRequest , BusinessMember businessMember) {
            this.businessMember = businessMember;
            this.dateContract = contractRequest.getDateContract();
            this.payType = contractRequest.getPayType();
            this.payPrice = contractRequest.getPayPrice();
            this.dateWorkStart = contractRequest.getDateWorkStart();
            this.dateWorkEnd = contractRequest.getDateWorkEnd();
            this.restTime = contractRequest.getRestTime();
            this.insurance = contractRequest.getInsurance();
            this.workDay = contractRequest.getWorkDay();
            this.workTime = contractRequest.getWorkTime();
            this.weekWorkTime = contractRequest.getWeekWorkTime();
            this.isWeekPay = contractRequest.getIsWeekPay();
            this.mealPay = contractRequest.getMealPay();
            this.wagePayment = contractRequest.getWagePayment();
            this.wageAccountNumber = contractRequest.getWageAccountNumber();
            this.contractCopyImgUrl = contractRequest.getContractCopyImgUrl();
        }
        @Override
        public Contract build() {
            return new Contract(this);
        }
    }
}
