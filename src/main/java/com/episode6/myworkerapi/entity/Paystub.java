package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.enums.PayType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Paystub {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "contractId")
    @ManyToOne(fetch = FetchType.LAZY)
    private Contract contract;

    @JoinColumn(name = "scheduleId")
    @ManyToOne(fetch = FetchType.LAZY)
    private Schedule schedule;

    @Column(nullable = false)
    private LocalDate dateStartWork;

    @Column(nullable = false)
    private LocalDate dateEndWork;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private PayType payType;

    @Column(nullable = false)
    private Double payBasic;

    private Double weekRestPay;

    private Double timeNightWork;

    private Double plusPay;

    private Double minusPay;

    private Double taxBeforePay;

    private Double taxPrice;

    private Double taxAfterPay;
}
