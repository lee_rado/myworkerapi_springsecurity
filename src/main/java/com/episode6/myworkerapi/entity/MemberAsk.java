package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.memberask.MemberAskRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberAsk {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId" , nullable = false)
    private Member member;

    @Column(nullable = false)
    private Boolean isAnswer;

    @Column(nullable = false, length = 30)
    private String title;

    @Column(nullable = false , columnDefinition = "TEXT")
    private String content;

    @Column()
    private String questionImgUrl;

    @Column(nullable = false)
    private LocalDateTime dateMemberAsk;

    public void putIsAnswer() {
        this.isAnswer = true;
    }

    private MemberAsk(Builder builder) {
        this.member = builder.member;
        this.isAnswer = builder.isAnswer;
        this.title = builder.title;
        this.content = builder.content;
        this.questionImgUrl = builder.questionImgUrl;
        this.dateMemberAsk = builder.dateMemberAsk;

    }

    public static class Builder implements CommonModelBuilder<MemberAsk> {
        private final Member member;
        private final Boolean isAnswer;
        private final String title;
        private final String content;
        private final String questionImgUrl;
        private final LocalDateTime dateMemberAsk;

        public Builder(Member member,MemberAskRequest request) {
            this.member = member;
            this.isAnswer = false;
            this.title = request.getTitle();
            this.content = request.getContent();
            this.questionImgUrl = request.getQuestionImgUrl();
            this.dateMemberAsk = LocalDateTime.now();
        }
        @Override
        public MemberAsk build() {
            return new MemberAsk(this);
        }
    }
}
