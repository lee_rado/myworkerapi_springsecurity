package com.episode6.myworkerapi.entity;


import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.schedule.ScheduleEndWorkRequest;
import com.episode6.myworkerapi.model.schedule.ScheduleRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "businessMemberId" , nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private BusinessMember businessMember;

    @Column(nullable = false)
    private LocalDate dateWork;

    @Column(nullable = false)
    private LocalTime startWork;

    /**
     * 위도 , 경도에 글자수 제한 ..?
     */
    @Column(nullable = false)
    private Double startLatitude;

    @Column(nullable = false)
    private Double startLongitude;

    private LocalTime endWork;

    private Double endLatitude;

    private Double endLongitude;

    private LocalTime restStartTime;

    private LocalTime restEndTime;

    private Double restTime;

    private Double plusWorkTime;

    private Double outWorkTime;

    private Double totalWorkTime;

    private Boolean isLateness;

    private Boolean isOverTime;

    @Column(length = 50)
    private String etc;

    public void putRecessStart() {
        this.restStartTime = LocalTime.now();
    }

    public void putEndWork(ScheduleEndWorkRequest request) {
        this.endWork = LocalTime.now();
        this.endLatitude = request.getEndLatitude();
        this.endLongitude = request.getEndLongitude();
        this.totalWorkTime = request.getTotalWorkTime();
        this.isOverTime = request.getIsOverTime();
        this.plusWorkTime = request.getPlusWorkTime();
    }

    private Schedule(Builder builder) {
        this.businessMember = builder.businessMember;
        this.dateWork = builder.dateWork;
        this.startWork = builder.startWork;
        this.startLatitude = builder.startLatitude;
        this.startLongitude = builder.startLongitude;
        this.etc = builder.etc;
        this.isLateness = builder.isLateness;
    }

    public static class Builder implements CommonModelBuilder<Schedule> {
        private final BusinessMember businessMember;
        private final LocalDate dateWork;
        private final LocalTime startWork;
        private final Double startLatitude;
        private final Double startLongitude;
        private final String etc;
        private final Boolean isLateness;

        public Builder(BusinessMember businessMember, ScheduleRequest request) {
            this.businessMember = businessMember;
            this.dateWork = LocalDate.now();
            this.startWork = LocalTime.now();
            this.startLatitude = request.getStartLatitude();
            this.startLongitude = request.getStartLongitude();
            this.etc = request.getEtc();
            this.isLateness = request.getIsLateness();
        }
        @Override
        public Schedule build() {
            return new Schedule(this);
        }
    }
}
