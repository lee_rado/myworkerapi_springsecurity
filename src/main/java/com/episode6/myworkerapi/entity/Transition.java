package com.episode6.myworkerapi.entity;

import com.episode6.myworkerapi.interfaces.CommonModelBuilder;
import com.episode6.myworkerapi.model.transition.TransitionRequest;
import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Transition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "businessMemberId")
    @ManyToOne(fetch = FetchType.LAZY)
    private BusinessMember businessMember;

    @Column(nullable = false)
    private LocalDateTime dateTransition;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String content;

    @Column(nullable = false)
    private Boolean isFinish;

    private LocalDateTime dateFinish;

    public void putContent(TransitionRequest request) {
        this.content = request.getContent();
    }

    public void putFinish() {
        this.isFinish = true;
        this.dateFinish = LocalDateTime.now();
    }

    private Transition(Builder builder) {
        this.businessMember = builder.businessMember;
        this.dateTransition = builder.dateTransition;
        this.content = builder.content;
        this.isFinish = builder.isFinish;
        this.dateFinish = builder.dateFinish;
    }

    public static class Builder implements CommonModelBuilder<Transition> {
        private final BusinessMember businessMember;
        private final LocalDateTime dateTransition;
        private final String content;
        private final Boolean isFinish;
        private final LocalDateTime dateFinish;

        public Builder(BusinessMember businessMember, TransitionRequest request) {
            this.businessMember = businessMember;
            this.dateTransition = LocalDateTime.now();
            this.content = request.getContent();
            this.isFinish = false;
            this.dateFinish = null;
        }
        @Override
        public Transition build() {
            return new Transition(this);
        }
    }
}
