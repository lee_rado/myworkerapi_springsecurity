package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter

public enum Position {
    MANAGER("매니저"),
    TEAM_LEADER("팀리더"),
    SENIOR_STAFF("월급제 알바생"),
    STAFF("시급제 알바생"),
    YOU_FIRED("퇴사");

    private final String name;
}
