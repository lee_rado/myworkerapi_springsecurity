package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Insurance {
    BASIC_INSURANCE("기본공제",false,0D,0D,0D,0D,0.033,0.033),
    FOUR_INSURANCE("4대공제", true, 0.03545,0.003545,0.009,0.045,0.033,0.125995);

    private final String name;
    private final Boolean isFourInsurance;
    private final Double healthInsurance;
    private final Double careInsurance;
    private final Double hireInsurance;
    private final Double pension;
    private final Double incomeTax;
    private final Double total;

}

