package com.episode6.myworkerapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MemberType {
    // 권한의 타입은 스트링이다.
    // 스트링은 형식이 없어서 스트링을 찾을 때 규칙을 추가해 검사를 해주기 위해 ROLE을 붙여서 작성한다.
    // 실질적으로 사용할때는 ROLE_를 제외한 값을 사용한다.
    ROLE_GENERAL("일반회원"),
    ROLE_BOSS("사업자"),
    ROLE_MANAGEMENT("관리자");

    private final String name;
}
