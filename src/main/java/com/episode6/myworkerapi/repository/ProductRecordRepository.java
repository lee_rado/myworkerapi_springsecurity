package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Product;
import com.episode6.myworkerapi.entity.ProductRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRecordRepository extends JpaRepository<ProductRecord, Long> {
    List<ProductRecord> findByProduct(Product product);
}
