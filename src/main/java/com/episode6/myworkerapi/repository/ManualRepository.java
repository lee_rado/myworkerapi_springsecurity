package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Manual;
import com.episode6.myworkerapi.model.manual.ManualItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManualRepository extends JpaRepository<Manual, Long> {

    Page<Manual> findAllByOrderByIdDesc(Pageable pageable);


    Page<Manual> findByBusinessMember_BusinessIdOrderByIdDesc(Pageable pageable, long id);
}
