package com.episode6.myworkerapi.repository;

import com.episode6.myworkerapi.entity.Paystub;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaystubRepository extends JpaRepository<Paystub, Long> {
}
